# Dear Employee,

TL;DR

I am a second year completed SLIIT undergraduate. Until now, from college I have learnt
PHP, Java, C++ and Javascript language wise and Data Structures and Algorithms.
For semester's projects, Book store(PHP & MYSQL), Vehicle Rent System(Java, Swing & MySQL),
Limbcare HMS(For a real client using Laravel & MySQL) and StudyMate(Android SDK & SQLite).

SLIIT is not the start but when I am doing OL, I learnt VB, HTML & CSS and for Advanced
Level I did ICT and there also I learnt HTML & CSS, MySQL and Python. Codecademy,
thenewboston youtube channel and etc. helped me a lot.

After completed Second year First sem. I realized that everything going fast and I cannot
work on specific language and I decided to postpond the semester(Also financial problem
arised).

Then I started learning  on Java 8 features, Spring Framework and also Hibernate ORM.
After a week I heard Google released Flutter UI framework. I realized that is the
opportunity for me to do some youtube videos and earn stackoverflow reputation. Those
gone well at that time and for my unfortunate my grand father died and those work stopped
from their and my chances are gone.

In 2018, I started 2nd year 2nd Sem. This was my greatest semester. I learned a alot.
Specially, before this sem I am the only one did the whole project and in this sem.
I did the Android app alone because I knew Android(Firstly learnt in 2016) and I taught
other three members and push the project to github and taught that also. I knew github
also but in this sem. learned how to work with fork, pull requests and lot about branches.
We had another subject called ITP that subject is awesome and it is a project that we are
doing for a real client. Project proposal, prototype, progress presentation, testing activity
and much more. We had a chance to decide which language to use and that was the hardest
part. I was the best in team and may be one of the best in 2nd year :D. I wanted to go for Angular
and Node.js but others didn't like it and they didn't know Javascript, some team mates
wanted to do it using vanila PHP but I didn't like PHP and two other members also, because of
that we chose Laravel to take at least one step forward. Others also agreed and we
together did a great project. Five members out of eight didn't have much knowledge in
programming and three of us teach them and get them to the track and finally we all happy :)

Last month, I completed the sem. and now I'm learning Design Patterns, Angular, React,
Node.js & Express.js, MongoDB, Postgres, Heroku, little bit Laravel, Dart & Flutter,
some more Android, little bit about Jqeury & AJAX, Because of lot of companies hiring C# .Net 
trying to learn that also. I liked to learn  GraphQL, Blockchain and ML. But my current focus 
is to get an internship.

The thing is I can learn anything and trying to learn all of these will not make me better
at one specific language in short time. So, if your company has a internship opportunity
please consider my request, If I have one specific tech stack to focus on, I will get
better at it. Documentations are well enough for me to learn. I love coding
and anything related to SE.

If your company hire me as a intern, I will work hard and anything that required to be a
better employee. Thanks for reading!

- Github: https://github.com/blasanka
- Stack Overflow: https://stackoverflow.com/users/3675035/blasanka
- My online Resume: https://blasanka.github.io/my_resume

Best Regards!

